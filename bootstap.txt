Git global setup

git config --global user.name "Rithika chy"
git config --global user.email "rithikachy45@gmail.com"

Create a new repository

git clone https://Rithikachy@gitlab.com/Rithikachy/rithikachy_162813_session6_b57_bootstrap.git
cd rithikachy_162813_session6_b57_bootstrap
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master

Existing folder

cd existing_folder
git init
git remote add origin https://Rithikachy@gitlab.com/Rithikachy/rithikachy_162813_session6_b57_bootstrap.git
git add .
git commit
git push -u origin master

Existing Git repository

cd existing_repo
git remote add origin https://Rithikachy@gitlab.com/Rithikachy/rithikachy_162813_session6_b57_bootstrap.git
git push -u origin --all
git push -u origin --tags